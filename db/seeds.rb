# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Club.create(name: 'Coco Cabana', street_address: 'Address 1')
Club.create(name: 'Night Spot', street_address: 'Address 2')

Band.create(name: 'Lancaster Crew', num_members: 2)
Band.create(name: 'Sinatra Duo', num_members: 3)
Band.create(name: 'Rockers', num_members: 5)
Band.create(name: 'Swingers', num_members: 2)
