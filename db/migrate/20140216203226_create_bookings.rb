class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.float :fee
      t.date :date
      t.references :club, index: true
      t.references :band, index: true

      t.timestamps
    end
  end
end
