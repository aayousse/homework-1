json.array!(@bands) do |band|
  json.extract! band, :id, :name, :num_members
  json.url band_url(band, format: :json)
end
