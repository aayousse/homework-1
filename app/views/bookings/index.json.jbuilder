json.array!(@bookings) do |booking|
  json.extract! booking, :id, :fee, :date, :club_id, :band_id
  json.url booking_url(booking, format: :json)
end
