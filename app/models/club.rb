class Club < ActiveRecord::Base
  validates :name, presence: true
  validates :street_address, presence: true

  has_many :bookings
  has_many :bands, through: :bookings, :uniq => true

end
