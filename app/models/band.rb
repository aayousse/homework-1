class Band < ActiveRecord::Base
  validates :name, presence: true
  validates :num_members, presence: true

  has_many :bookings
  has_many :clubs, through: :bookings, :uniq => true

end
