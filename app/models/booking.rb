class Booking < ActiveRecord::Base
belongs_to :club
belongs_to :band

validates :fee, presence:true, numericality: {greater_than: 0}
validates :date, presence: true
validates :club_id, presence: true
validates :band_id, presence: true

end
